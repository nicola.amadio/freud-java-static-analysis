package demo.classes;

public class FizzBuzz {

    public void printFizzBuzz(int k){
        int l = 5;
        if (k%15==0)
            System.out.println("analyze.FizzBuzz");
        else if (k%5==0)
            System.out.println("Buzz");
        else if (k%3==0)
            System.out.println("Fizz");
        else
            System.out.println(k);
    }

    public void fizzBuzz(int n){
        for (int i=1; i<=n; i++)
            printFizzBuzz(i);
    }
}

